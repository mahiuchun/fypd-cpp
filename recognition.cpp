#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include <syslog.h>

#include <opencv2/opencv.hpp>

#include "targetsmgmt.hpp"

using namespace cv;
using namespace std;

bool createDetectorDescriptorMatcher( const string& detectorType, const string& descriptorType, const string& matcherType,
                                      Ptr<FeatureDetector>& featureDetector,
                                      Ptr<DescriptorExtractor>& descriptorExtractor,
                                      Ptr<DescriptorMatcher>& descriptorMatcher )
{
    featureDetector = FeatureDetector::create( detectorType );
    featureDetector->set("hessianThreshold", 400);
    descriptorExtractor = DescriptorExtractor::create( descriptorType );
    descriptorMatcher = DescriptorMatcher::create( matcherType );

    bool isCreated = featureDetector && descriptorExtractor && descriptorMatcher;
    if( !isCreated )
        cerr << "Can not create feature detector or descriptor extractor or descriptor matcher of given types." << endl;

    return isCreated;
}

static void find_good_matches(vector<DMatch> &matches, vector<DMatch> &good_matches)
{
    double max_dist = 0, min_dist = 100;
    for ( int i = 0; i < matches.size(); i++ ) {
        double dist = matches[i].distance;
        if ( dist < min_dist ) min_dist = dist;
        if ( dist > max_dist ) max_dist = dist;
    }
    for ( int i = 0; i < matches.size(); i++ ) {
        if ( matches[i].distance < 3*min_dist ) {
	    good_matches.push_back( matches[i]);
	}
    }
}

static int find_popular_matches(vector<DMatch> &matches, vector<DMatch> &popular_matches)
{
    unordered_map<int, int> counts;
    // record counts
    for ( int i = 0; i < matches.size(); i++ ) {
        int index = matches[i].imgIdx;
        counts[index]++;
    }
    // find max k/v
    int maxk = -1;
    int maxv = 0;
    for (auto &kv: counts) {
        if (kv.second > maxv) {
            maxv = kv.second;
            maxk = kv.first;
        }
    }
    // filter out other matches
    for ( int i = 0; i < matches.size(); i++ ) {
        if (matches[i].imgIdx == maxk) {
            popular_matches.push_back(matches[i]);
        }
    }
    return maxk;
}

static int best_match(vector<DMatch> &matches, vector<DMatch> &good_matches)
{
    vector<DMatch> t;
#define VOTE_FIRST 0
#if VOTE_FIRST
    int imgIdx = find_popular_matches(matches, t);
    find_good_matches(t, good_matches);
#else
    find_good_matches(matches, t);
    int imgIdx = find_popular_matches(t, good_matches);
#endif
    return imgIdx;
}

static bool homography_test(vector<KeyPoint> &keypoints_object, vector<KeyPoint> &keypoints_scene,
                            vector<DMatch> &good_matches,
                            Mat &img_object, Mat &img_scene)
{
    const double MINAREA_RECT_THRESHOLD = 0.65;

    vector<Point2f> obj;
    vector<Point2f> scene;

    for ( int i = 0; i < good_matches.size(); i++ ) {
        obj.push_back( keypoints_object[ good_matches[i].trainIdx ].pt );
        scene.push_back( keypoints_scene[ good_matches[i].queryIdx ].pt );
    }

    if (obj.empty() || scene.empty()) {
        return true;
    }

    std::vector<Point2f> obj_corners(4);
    std::vector<Point2f> scene_corners(4);

    try {
        Mat H = findHomography( obj, scene, CV_RANSAC );
        obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( img_object.cols, 0 );
        obj_corners[2] = cvPoint( img_object.cols, img_object.rows ); obj_corners[3] = cvPoint( 0, img_object.rows );
        perspectiveTransform( obj_corners, scene_corners, H);
    } catch (Exception &e) {
        syslog(LOG_ERR, "%s", e.what());
        return false;
    }

    auto rotatedRect = minAreaRect(scene_corners);
    double obj_area = contourArea(scene_corners);
    double rect_area = rotatedRect.size.width * rotatedRect.size.height;
    return obj_area/rect_area > MINAREA_RECT_THRESHOLD;
}

void preprocessing(Mat &src, Mat &dst)
{
    const int kernel_length = 5;
    int i = kernel_length;
    bilateralFilter ( src, dst, i, i*2, i/2 );
}

void prepare_masks(const Mat &queryDescriptors, vector<Mat> &masks)
{
    auto trainDescriptors = descriptorMatcher->getTrainDescriptors();
    for (int i = 0; i < trainMasks.size(); i++) {
        if (trainMasks[i]) {
            masks.push_back(Mat());
        } else {
            masks.push_back(Mat::zeros(queryDescriptors.rows, trainDescriptors[i].rows, CV_8U));
        }
    }
}

int recognize(const string& filename)
{
    int error;
    if (error = pthread_rwlock_rdlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_rdlock %m");
        return -1;
    }

    Mat origImage, queryImage;
    origImage = imread( filename, IMREAD_GRAYSCALE );
    preprocessing(origImage, queryImage);

    vector<KeyPoint> queryKeypoints;
    featureDetector->detect( queryImage, queryKeypoints );

    Mat queryDescriptors;
    descriptorExtractor->compute( queryImage, queryKeypoints, queryDescriptors );

    vector<Mat> masks;
    prepare_masks( queryDescriptors, masks );

    vector<DMatch> matches;
    descriptorMatcher->match( queryDescriptors, matches, masks );

    vector<DMatch> good_matches;
    int imgIdx = best_match( matches, good_matches );
    int imgIdxSaved = imgIdx;

    if (imgIdx >= 0) {
        bool r = homography_test(trainKeypoints[imgIdx], queryKeypoints, good_matches, trainImages[imgIdx], queryImage);
        if (!r) imgIdx = -1;
    }

    if (error = pthread_rwlock_unlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_unlock %m");
        return -1;
    }

    return imgIdx;
}
