#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <streambuf>
#include <string>

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <syslog.h>
#include <unistd.h>

#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/nonfree.hpp>

#include "become_daemon.h"
#include "recognition.hpp"
#include "targetsmgmt.hpp"

const std::string defaultDetectorType = "SURF";
const std::string defaultDescriptorType = "SURF";
const std::string defaultMatcherType = "BruteForce";

ssize_t sendall(int sockfd, const void *buf, size_t len, int flags)
{
    size_t total = 0;
    ssize_t n;
    while (total < len) {
        n = send(sockfd, (void*)(((char*)buf)+total), len-total, flags);
        if (n == -1 && errno != EINTR) {
            break;
        }
        total += n;
    }
    if (n == -1) {
        return -1;
    } else {
        return total;
    }
}

void* start_routine(void* data) {
    int id = -1;
    int *sock_ptr = (int*)data;
    int sock = *sock_ptr;
    delete sock_ptr;
    int len = 0;
    char digit;
    while (1) {
        recv(sock, &digit, 1, MSG_WAITALL);
        if (digit == ':') break;
        len = len * 10 + digit - '0';
    }
    void* in_buf = malloc(len);
    recv(sock, in_buf, len, MSG_WAITALL);
    recv(sock, &digit, 1, MSG_WAITALL);
    char filename[PATH_MAX];
    strcpy(filename, "/tmp/fypdXXXXXX");
    int fd = mkstemp(filename);
    write(fd, in_buf, len);
    close(fd);
    id = recognize(filename);
    unlink(filename);
    free(in_buf);
    std::string json;
    if (id >= 0) {
         std::string json_filename = reco_targets[id]+".json";
         std::ifstream t(media_dir+json_filename);
         if (t.good()) {
             json.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
         } else {
             json = "{\"id\":-1}";
         }
    } else {
         json = "{\"id\":-1}";
    }
    sendall(sock, json.c_str(), json.size(), 0);
    close(sock);
    return nullptr;
}

void* recognition_server(void* data)
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in my_addr;
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(1234);
    my_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    int optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
    if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof my_addr) == -1) {
        syslog(LOG_ERR, "recognition bind: %m");
        exit(EXIT_FAILURE);
    }
    if (listen(sockfd, 10) == -1) {
        syslog(LOG_ERR, "recognition listen: %m");
        exit(EXIT_FAILURE);
    }
    int sock;
    pthread_t thread;
    while ((sock = accept(sockfd, NULL, NULL)) != -1) {
        int* sock_ptr = new int;
        *sock_ptr = sock;
        pthread_create(&thread, NULL, start_routine, (void*)sock_ptr);
    }
    close(sockfd);
    return nullptr;
}

void* targetsmgmt_server(void* data)
{
    int sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
    struct sockaddr_un my_addr;
    my_addr.sun_family = AF_UNIX;
    strcpy(my_addr.sun_path, "/tmp/fypd-targetsmgmt");
    unlink(my_addr.sun_path);
    int optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
    if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof my_addr) == -1) {
        syslog(LOG_ERR, "targetsmgmt bind: %m");
        exit(EXIT_FAILURE);
    }
    if (listen(sockfd, 10) == -1) {
        syslog(LOG_ERR, "targetsmgmt listen: %m");
        exit(EXIT_FAILURE);
    }
    chmod(my_addr.sun_path, 0777);
    int sock;
    const int MSG_SIZE = 43;
    char buf[MSG_SIZE+1];
    buf[MSG_SIZE] = 0;
    std::string msg_ok(MSG_SIZE, ' ');
    msg_ok[0] = 'O';
    msg_ok[1] = 'K';
    std::string msg_err(MSG_SIZE, ' ');
    msg_err[0] = 'E';
    msg_err[1] = 'R';
    msg_err[2] = 'R';
    while ((sock = accept(sockfd, NULL, NULL)) != -1) {
        recv(sock, buf, MSG_SIZE, MSG_WAITALL);
        std::string cmd(buf, 3);
        std::string sha1(buf+3);
        if (cmd == "ADD") {
            add_target(sha1);
            sendall(sock, msg_ok.c_str(), MSG_SIZE, 0);
        } else if (cmd == "DEL") {
            del_target(sha1);
            sendall(sock, msg_ok.c_str(), MSG_SIZE, 0);
        } else if (cmd == "LST") {
            std::vector<std::string> targets;
            lst_targets(targets);
            for (std::string &sha1: targets) {
                std::string msg_ent = "ENT"+sha1;
                sendall(sock, msg_ent.c_str(), MSG_SIZE, 0);
            }
            sendall(sock, msg_ok.c_str(), MSG_SIZE, 0);
        } else if (cmd == "STA") {
            int enabled = 0;
            int disabled = 0;
            targets_stat(enabled, disabled);
            std::string msg_sta = "STA[" + std::to_string(enabled) + "," + std::to_string(disabled) + "]";
            while (msg_sta.length() < MSG_SIZE) {
                msg_sta.append(" ");
            }
            sendall(sock, msg_sta.c_str(), MSG_SIZE, 0);
            sendall(sock, msg_ok.c_str(), MSG_SIZE, 0);
        }
        close(sock);
    }
    close(sockfd);
    return nullptr;
}

int main(int argc, char **argv)
{
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
        perror("signal");
        exit(EXIT_FAILURE);
    }
    cv::initModule_nonfree();
    std::string detectorType = defaultDetectorType;
    std::string descriptorType = defaultDescriptorType;
    std::string matcherType = defaultMatcherType;
    if( !createDetectorDescriptorMatcher( detectorType, descriptorType, matcherType, featureDetector, descriptorExtractor, descriptorMatcher ) ) {
        exit(EXIT_FAILURE);
    }
    if (argc == 2 && std::string(argv[1]) == "-d") {
        if (becomeDaemon(0) == -1) {
            perror("becomeDaemon");
            exit(EXIT_FAILURE);
        }
    }
    openlog(argv[0], LOG_PID | LOG_CONS | LOG_NOWAIT, LOG_LOCAL0);
    int error;
    if (error = pthread_rwlock_init(&rwlock, NULL)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_init %m");
        exit(EXIT_FAILURE);
    }
    reload_targets();
    pthread_t thread1;
    pthread_t thread2;
    pthread_create(&thread1, NULL, recognition_server, NULL);
    pthread_create(&thread2, NULL, targetsmgmt_server, NULL);
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
}
