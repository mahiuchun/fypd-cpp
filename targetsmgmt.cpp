#include <string>
#include <unordered_map>
#include <vector>

#include <dirent.h>
#include <sys/types.h>
#include <syslog.h>
#include <pthread.h>

#include <boost/algorithm/string/predicate.hpp>
#include <opencv2/opencv.hpp>

std::string media_dir = "/var/www/fyp/static/media/";
std::vector<std::string> reco_targets;
cv::Ptr<cv::FeatureDetector> featureDetector;
cv::Ptr<cv::DescriptorExtractor> descriptorExtractor;
cv::Ptr<cv::DescriptorMatcher> descriptorMatcher;
std::vector<cv::Mat> trainImages;
std::vector<std::vector<cv::KeyPoint> > trainKeypoints;
std::vector<char> trainMasks;
pthread_rwlock_t rwlock;

std::unordered_map<std::string, int> targets_map;

int clear_targets();
int load_target(std::string filename);
int reload_targets();
int train_targets();

int add_target(std::string sha1)
{
    int error;
    if (error = pthread_rwlock_wrlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_wrlock %m");
        return -1;
    }

    load_target(sha1+".jpg");
    train_targets();

    if (error = pthread_rwlock_unlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_unlock %m");
        return -1;
    }

    return 0;
}

int del_target(std::string sha1)
{
    int error;
    if (error = pthread_rwlock_wrlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_wrlock %m");
        return -1;
    }

    if (targets_map.find(sha1) != targets_map.end()) {
        int i = targets_map[sha1];
        trainMasks[i] = 0;
    }

    if (error = pthread_rwlock_unlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_unlock %m");
        return -1;
    }

    return 0;
}

int lst_targets(std::vector<std::string> &targets)
{
    int error;
    if (error = pthread_rwlock_rdlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_rdlock %m");
        return -1;
    }

    targets.clear();
    for (int i = 0; i < reco_targets.size(); i++) {
        if (trainMasks[i]) {
            targets.push_back(reco_targets[i]);
        }
    }

    if (error = pthread_rwlock_unlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_unlock %m");
        return -1;
    }

    return 0;
}

int targets_stat(int &enabled, int &disabled)
{
    int error;
    if (error = pthread_rwlock_rdlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_rdlock %m");
        return -1;
    }

    enabled = 0;
    disabled = 0;
    for (int i = 0; i < trainMasks.size(); i++) {
        if (trainMasks[i]) {
            enabled++;
        } else {
            disabled++;
        }
    }

    if (error = pthread_rwlock_unlock(&rwlock)) {
        errno = error;
        syslog(LOG_ERR, "pthread_rwlock_unlock %m");
        return -1;
    }

    return 0;
}

int clear_targets()
{
    reco_targets.clear();
    targets_map.clear();
    trainImages.clear();
    trainKeypoints.clear();
    trainMasks.clear();
    return 0;
}

int load_target(std::string filename)
{
    std::string sha1 = filename.substr(0, 40);
    if (targets_map.find(sha1) != targets_map.end()) {
        trainMasks[targets_map[sha1]] = 1;
        return 0;
    }

    std::string fullpath = media_dir + filename;
    cv::Mat img = cv::imread( fullpath, cv::IMREAD_GRAYSCALE );
    if ( img.empty() ) {
        syslog(LOG_ERR, "Train image %s can not be read.", filename.c_str());
        return -1;
    }
    trainImages.push_back( img );

    std::vector<cv::KeyPoint> imgKeypoints;
    featureDetector->detect( img, imgKeypoints );
    trainKeypoints.push_back( imgKeypoints );

    cv::Mat imgDescriptors;
    descriptorExtractor->compute( img, imgKeypoints, imgDescriptors );

    std::vector<cv::Mat> descriptors;
    descriptors.push_back(imgDescriptors);
    descriptorMatcher->add(descriptors);

    targets_map[sha1] = descriptorMatcher->getTrainDescriptors().size() - 1;
    reco_targets.push_back( sha1 );
    trainMasks.push_back(1);
    return 0;
}

int reload_targets()
{
    DIR *dirp = opendir(media_dir.c_str());
    if (dirp == nullptr) {
        syslog(LOG_ERR, "opendir: %m");
        return -1;
    }
    clear_targets();
    struct dirent *entry;
    while ((entry = readdir(dirp)) != NULL) {
        std::string filename(entry->d_name);
        if ( !boost::algorithm::ends_with(filename, ".jpg") ) {
            continue;
        }
        load_target(filename);
    }
    closedir(dirp);
    train_targets();
    return 0;
}

int train_targets()
{
    if (!descriptorMatcher->getTrainDescriptors().empty()) {
        descriptorMatcher->train();
    }
    return 0;
}
