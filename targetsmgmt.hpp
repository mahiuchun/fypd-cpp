#ifndef TARGETSMGMET_HPP
#define TARGETSMGMET_HPP

#include <string>
#include <vector>
#include <pthread.h>

extern std::string media_dir;
extern std::vector<std::string> reco_targets;
extern cv::Ptr<cv::FeatureDetector> featureDetector;
extern cv::Ptr<cv::DescriptorExtractor> descriptorExtractor;
extern cv::Ptr<cv::DescriptorMatcher> descriptorMatcher;
extern std::vector<cv::Mat> trainImages;
extern std::vector<std::vector<cv::KeyPoint> > trainKeypoints;
extern std::vector<char> trainMasks;
extern pthread_rwlock_t rwlock;

int add_target(std::string sha1);
int del_target(std::string sha1);
int lst_targets(std::vector<std::string> &targets);
int targets_stat(int &enabled, int &disabled);
int reload_targets();

#endif
