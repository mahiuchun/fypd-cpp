#ifndef RECOGNITION_HPP
#define RECOGNITION_HPP

#include <string>
#include <opencv2/opencv.hpp>

int recognize(const std::string& filename);
bool createDetectorDescriptorMatcher( const std::string& detectorType, const std::string& descriptorType, const std::string& matcherType,
                                      cv::Ptr<cv::FeatureDetector>& featureDetector,
                                      cv::Ptr<cv::DescriptorExtractor>& descriptorExtractor,
                                      cv::Ptr<cv::DescriptorMatcher>& descriptorMatcher );

#endif
